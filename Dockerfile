FROM debian:buster

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y python3-pip sshpass git openssh-client libhdf5-dev libssl-dev libffi-dev && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean
    
RUN pip3 install --upgrade pip cffi && \
    pip install ansible==2.9.27 && \
    rm -rf /root/.cache/pip

RUN mkdir /ansible &&  mkdir -p /etc/ansible

ADD inventory /etc/ansible/hosts
ADD ansible.cfg /etc/ansible/ansible.cfg
#WORKDIR /ansible
#RUN ansible all --private-key key  -m ping 

